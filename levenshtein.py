from similarity.cosine import Cosine

def cosine(s1, s2):
    cosine = Cosine(2)
    p1 = cosine.get_profile(s1)
    p2 = cosine.get_profile(s2)
    return cosine.similarity(p1, p2)

from similarity.jarowinkler import JaroWinkler

def jarowinkler(s1, s2):
    return JaroWinkler().similarity(s1, s2)

from similarity.ngram import NGram

def tenGram(s1, s2):
    return NGram(10).distance(s1, s2)

from similarity.jaccard import Jaccard

def jaccard(s1, s2):
    return Jaccard(1).similarity(s1, s2)

from similarity.metric_lcs import MetricLCS

def metric_lcs(s1, s2):
    return MetricLCS().distance(s1, s2)

from similarity.levenshtein import Levenshtein

def ls(s1, s2):
    return Levenshtein().distance(s1, s2)

from similarity.normalized_levenshtein import NormalizedLevenshtein

def nor_sim(s1, s2):
    return NormalizedLevenshtein().similarity(s1, s2)



