import levenshtein
from sklearn.svm import LinearSVC
import pandas as pd
import numpy as np
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression

data = pd.read_csv("amazon_ebay.txt", sep = "|")
data = np.array(data)
product_1 = data[:, 0]
product_2 = data[:, 1]
labels = data[:, 2]
labels = np.array(labels, dtype=int)
maxAcc = 0
algo = ""
n_needed = 0
all_r2 = []

N = 99
pre_process = []
for i in range(product_2.shape[0]):
    s1 = product_1[i]
    s2 = product_2[i]
    s1 = s1.lower()
    s2 = s2.lower()
    for char in ['~', '`', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '-', '+', '=', '{', '[', ']', '}', '|', '\\', ':', ':', '"', "'", '<', ',', '>', '.', '/', '?']:
        s1.replace(char, " ")
        s2.replace(char, " ")
    jaro = levenshtein.jarowinkler(s1, s2)
    jaccard = levenshtein.jarowinkler(s1, s2)
    tengram = levenshtein.tenGram(s1, s2)
    metric = levenshtein.metric_lcs(s1, s2)
    nor = levenshtein.nor_sim(s1, s2)
    pre_process.append([jaro, jaccard, metric])

train_data = pre_process[:N]
train_label = labels[:N]

test_data = pre_process[N:]
test_label = labels[N:]

lr = LogisticRegression(max_iter=500)
lr.fit(train_data, train_label)
r2 = accuracy_score(lr.predict(test_data), test_label)
if r2 > maxAcc:
    algo = "LogisticRegression"
    maxAcc = r2
    n_needed = N
all_r2.append([r2 * 100, N])

print("Max accuracy: " + str(maxAcc * 100) + "%")
print("Algorithm: " + str(algo))
print("Number of needed data: " + str(n_needed))